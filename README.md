# landing-page
The landing page for Nachbarschaftsmarktplatz.

`https://nachbarschaftsmarktplatz.de`

Built with bulma framework. (CDN)

# start a simple http server
* Install dependencies: `yarn`
* Start sever: `yarn start`
