const RETAIL = {
  '1': {
    'left': {
      'title': "",
      'body': "Trage deinen Laden auf der Karte ein.",
    },
    'right': {
      'title': '',
      'body': '',
    },
  },
  '2': {
    'left': {
      'title': '',
      'body': '',
    },
    'right': {
      'title': "",
      'body': "Erstelle deinen eigenen Online-Shop auf dem Marktplatz.",
    },
  },
  '3': {
    'left': {
      'title': "",
      'body': "Liefere die Bestellungen selbst aus oder stelle deinen Kund:innen alles zur Abholung bereit.",
    },
    'right': {
      'title': '',
      'body': '',
    },
  },
}

const SUPPORT = {
  '1': {
    'left': {
      'title': '',
      'body': '',
    },
    'right': {
      'title': "",
      'body': "Erkunde lokale Läden auf der Karte.",
    },
  },
  '2': {
    'left': {
      'title': "",
      'body': "Stöbere in den Online-Shops auf dem Marktplatz.",
    },
    'right': {
      'title': '',
      'body': '',
    },
  },
  '3': {
    'left': {
      'title': '',
      'body': '',
    },
    'right': {
      'title': "",
      'body': "Online einkaufen & einfach abholen oder liefern lassen.",
    },
  },
}

const buildContentMap = (data) => {
  const contentMap = new Map()

  for (const index in data) {
    for (const side in data[index]) {
      for (const type in data[index][side]) {
        const id = `${side}_${type}_${index}`

        contentMap.set(id, data[index][side][type])
      }
    }
  }

  return contentMap
}

const showContent = (data) => {
  const contentByElementId = buildContentMap(data)

  for (const [elementId, content] of contentByElementId) {
    document.getElementById(elementId).textContent = content
  }
}

const showSupportersContent = () => {
  showContent(SUPPORT)
}

const showRetailersContent = () => {
  showContent(RETAIL)
}

document.addEventListener('DOMContentLoaded', (event) => {
  //document.onload = showSupportersContent()
});

const toggleBurgerMenu = () => {
  document.querySelector('.navbar-menu').classList.toggle('is-active')
}
